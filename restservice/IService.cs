﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.IO;

namespace restservice
{
    [ServiceContract]
    public interface IService
    {
        #region aunit

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "login/{username}/{password}")]
        [return: MessageParameter(Name = "Data")]
        USER DoLogin(string username, string password);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "aunit/rBeliSuratJalanSupplier")]
        Stream rBeliSuratJalanSupplier(Stream rBeliSuratJalanSupplier);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "aunit/rBeliFakturBeli")]
        Stream rBeliFakturBeli(Stream rBeliFakturBeli);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "aunit/rJualInden")]
        Stream rJualInden(Stream rJualInden);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "aunit/rJualDataKonsumen")]
        Stream rJualDataKonsumen(Stream rJualDataKonsumen);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "aunit/rJualSuratJalanKonsumen")]
        Stream rJualSuratJalanKonsumen(Stream rJualSuratJalanKonsumen);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "aunit/rJualReturKonsumen")]
        Stream rJualReturKonsumen(Stream rJualReturKonsumen);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "aunit/rMutasiSDealer")]
        Stream rMutasiSDealer(Stream rMutasiSDealer);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "aunit/rMutasiSPOS")]
        Stream rMutasiSPOS(Stream rMutasiSPOS);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "aunit/rMutasiPDealer")]
        Stream rMutasiPDealer(Stream rMutasiPDealer);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "aunit/rMutasiPPos")]
        Stream rMutasiPPos(Stream rMutasiPPos);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "aunit/rKeuanganKasMasuk")]
        Stream rKeuanganKasMasuk(Stream rKeuanganKasMasuk);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "aunit/rKeuanganKasKeluar")]
        Stream rKeuanganKasKeluar(Stream rKeuanganKasKeluar);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "aunit/rKeuanganBankMasuk")]
        Stream rKeuanganBankMasuk(Stream rKeuanganBankMasuk);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "aunit/rKeuanganBankKeluar")]
        Stream rKeuanganBankKeluar(Stream rKeuanganBankKeluar);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "aunit/rKeuanganSaldoKasBank")]
        Stream rKeuanganSaldoKasBank(Stream rKeuanganSaldoKasBank);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "aunit/rMotorDaftarMotor")]
        Stream rMotorDaftarMotor(Stream rMotorDaftarMotor);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "aunit/rMotorSTNKBPKB")]
        Stream rMotorSTNKBPKB(Stream rMotorSTNKBPKB);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "aunit/rMotorRiwayatMotor")]
        Stream rMotorRiwayatMotor(Stream rMotorRiwayatMotor);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "aunit/rMotorStockMotorFisik")]
        Stream rMotorStockMotorFisik(Stream rMotorStockMotorFisik);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "aunit/rMarketRekapHarian")]
        Stream rMarketRekapHarian(Stream rMarketRekapHarian);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "aunit/rMarketRekapBulanan")]
        Stream rMarketRekapBulanan(Stream rMarketRekapBulanan);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "aunit/rMarketPivotTabel")]
        Stream rMarketPivotTabel(Stream rMarketPivotTabel);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "aunit/rMarketDashboard1")]
        Stream rMarketDashboard1(Stream rMarketDashboard1);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "aunit/rMarketDashboard2")]
        Stream rMarketDashboard2(Stream rMarketDashboard2);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "aunit/rMarketDashboard3")]
        Stream rMarketDashboard3(Stream rMarketDashboard3);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "aunit/rAstra")]
        Stream rAstra(Stream rAstra);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "aunit/rData")]
        Stream rData(Stream rData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "aunit/rUserLog")]
        Stream rUserLog(Stream rUserLog);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "aunit/rUGLJurnalUmum")]
        Stream rUGLJurnalUmum(Stream rUGLJurnalUmum);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "aunit/rUGLNeracaPercobaan")]
        Stream rUGLNeracaPercobaan(Stream rUGLNeracaPercobaan);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "aunit/rUGLNeracaRugiLaba")]
        Stream rUGLNeracaRugiLaba(Stream rUGLNeracaRugiLaba);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "aunit/rUGLHarian")]
        Stream rUGLHarian(Stream rUGLHarian);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "aunit/rUGLPiutang")]
        Stream rUGLPiutang(Stream rUGLPiutang);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "aunit/rUGLHutang")]
        Stream rUGLHutang(Stream rUGLHutang);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "aunit/rGLDashboard")]
        Stream rGLDashboard(Stream rGLDashboard);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "aunit/rGLStockMotor")]
        Stream rGLStockMotor(Stream rGLStockMotor);

        #region Print Kwitansi
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "aunit/fss")]
        Stream fss(Stream fss);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "aunit/ffb")]
        Stream ffb(Stream ffb);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "aunit/fsd")]
        Stream fsd(Stream fsd);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "aunit/fin")]
        Stream fin(Stream fin);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "aunit/fdk")]
        Stream fdk(Stream fdk);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "aunit/fsk")]
        Stream fsk(Stream fsk);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "aunit/frk")]
        Stream frk(Stream frk);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "aunit/fkm")]
        Stream fkm(Stream fkm);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "aunit/fkk")]
        Stream fkk(Stream fkk);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "aunit/fpd")]
        Stream fpd(Stream fpd);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "aunit/fsmhd")]
        Stream fsmhd(Stream fsmhd);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "aunit/fpbhd")]
        Stream fpbhd(Stream fpbhd);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "aunit/fabhd")]
        Stream fabhd(Stream fabhd);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "aunit/fbkhd2")]
        Stream fbkhd2(Stream fbkhd2);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "aunit/fbmunit")]
        Stream fbmunit(Stream fbmunit);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "aunit/fgeneralledgerhd")]
        Stream fgeneralledgerhd(Stream fgeneralledgerhd);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "aunit/fskkolektif")]
        Stream fskkolektif(Stream fskkolektif);

        

        #endregion

        #endregion


        #region abengkel
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "abengkel/rPenjualanInvoice")]
        Stream rPenjualanInvoice(Stream rPenjualanInvoice);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "abengkel/rServisDraft")]
        Stream rServisDraft(Stream rServisDraft);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "abengkel/rServisInvoice")]
        Stream rServisInvoice(Stream rServisInvoice);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "abengkel/rServisEstimasi")]
        Stream rServisEstimasi(Stream rServisEstimasi);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "abengkel/rKlaimKPB")]
        Stream rKlaimKPB(Stream rKlaimKPB);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "abengkel/rKlaimC2")]
        Stream rKlaimC2(Stream rKlaimC2);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "abengkel/rPickingList")]
        Stream rPickingList(Stream rPickingList);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "abengkel/rPembelianOrder")]
        Stream rPembelianOrder(Stream rPembelianOrder);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "abengkel/rPembelianInvoice")]
        Stream rPembelianInvoice(Stream rPembelianInvoice);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "abengkel/rPenerimaanBarang")]
        Stream rPenerimaanBarang(Stream rPenerimaanBarang);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "abengkel/rPembelianRetur")]
        Stream rPembelianRetur(Stream rPembelianRetur);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "abengkel/rPenjualanOrder")]
        Stream rPenjualanOrder(Stream rPenjualanOrder);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "abengkel/rPenjualanRetur")]
        Stream rPenjualanRetur(Stream rPenjualanRetur);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "abengkel/rTransfer")]
        Stream rTransfer(Stream rTransfer);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "abengkel/rPenyesuaian")]
        Stream rPenyesuaian(Stream rPenyesuaian);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "abengkel/rKas")]
        Stream rKas(Stream rKas);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "abengkel/rBank")]
        Stream rBank(Stream rBank);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "abengkel/rGLJurnalUmum")]
        Stream rGLJurnalUmum(Stream rGLJurnalUmum);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "abengkel/rGLNeracaPercobaan")]
        Stream rGLNeracaPercobaan(Stream rGLNeracaPercobaan);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "abengkel/rGLNeracaRugiLaba")]
        Stream rGLNeracaRugiLaba(Stream rGLNeracaRugiLaba);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "abengkel/rGLPiutang")]
        Stream rGLPiutang(Stream rGLPiutang);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "abengkel/rGLHutang")]
        Stream rGLHutang(Stream rGLHutang);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "abengkel/rGLHarian")]
        Stream rGLHarian(Stream rGLHarian);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "abengkel/rGLBulanan")]
        Stream rGLBulanan(Stream rGLBulanan);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "abengkel/rStockItem")]
        Stream rStockItem(Stream rStockItem);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "abengkel/rStockKartu")]
        Stream rStockKartu(Stream rStockKartu);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "abengkel/rMekanikPivot")]
        Stream rMekanikPivot(Stream rMekanikPivot);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "abengkel/rMekanikRLH")]
        Stream rMekanikRLH(Stream rMekanikRLH);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "abengkel/rMekanikStartStop")]
        Stream rMekanikStartStop(Stream rMekanikStartStop);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "abengkel/rBData")]
        Stream rBData(Stream rData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "abengkel/rBUseLog")]
        Stream rBUseLog(Stream rUseLog);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "abengkel/rQuery")]
        Stream rBQuery(Stream rQuery);

        #region PrintOut Kwitansi
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "abengkel/fsdhd")]
        Stream fsdhd(Stream fsdhd);
        
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "abengkel/fckhd")]
        Stream fckhd(Stream fckhd);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "abengkel/fcchd")]
        Stream fcchd(Stream fcchd);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "abengkel/fsohd")]
        Stream fsohd(Stream fsohd);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "abengkel/fsihd")]
        Stream fsihd(Stream fsihd);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "abengkel/fsrhd")]
        Stream fsrhd(Stream fsrhd);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "abengkel/fpohd")]
        Stream fpohd(Stream fpohd);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "abengkel/fpshd")]
        Stream fpshd(Stream fpshd);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "abengkel/fpihd")]
        Stream fpihd(Stream fpihd);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "abengkel/fprhd")]
        Stream fprhd(Stream fprhd);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "abengkel/ftahd")]
        Stream ftahd(Stream ftahd);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "abengkel/ftihd")]
        Stream ftihd(Stream ftihd);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "abengkel/fkmhd")]
        Stream fkmhd(Stream fkmhd);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "abengkel/fkkhd")]
        Stream fkkhd(Stream fkkhd);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "abengkel/fbmhd")]
        Stream fbmhd(Stream fbmhd);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "abengkel/fbkhd")]
        Stream fbkhd(Stream fbkhd);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "abengkel/fsehd")]
        Stream fsehd(Stream fbkhd);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "abengkel/plsd")]
        Stream plsd(Stream plsd);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, UriTemplate = "abengkel/plsi")]
        Stream plsi(Stream plsi);


        #endregion
        #endregion

    }

    public class USER
    {
        public string username { get; set; }
        public string password { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
    }
}
