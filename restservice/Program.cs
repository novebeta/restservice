﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Web;
using System.Text;
using System.Threading;
using CrystalDecisions.CrystalReports.Engine;
using System.Data;
using MySql.Data.MySqlClient;
//using Castle.DynamicProxy;

namespace restservice
{
    class Program
    {       

        static void Main(string[] args)
        {
            ServiceProc();
            //try
            //{
            //    WebServiceHost hostWeb = new WebServiceHost(typeof (restservice.Service));
            //    ServiceEndpoint ep = hostWeb.AddServiceEndpoint(typeof (restservice.IService), new WebHttpBinding(), "");
            //    ServiceThrottlingBehavior stb = hostWeb.Description.Behaviors.Find<ServiceThrottlingBehavior>();
            //    if (stb == null)
            //    {
            //        stb = new ServiceThrottlingBehavior();
            //        hostWeb.Description.Behaviors.Add(stb);
            //    }

            //    stb.MaxConcurrentCalls = 50;
            //    stb.MaxConcurrentInstances = 50;
            //    stb.MaxConcurrentSessions = 50;

            //    hostWeb.Open();
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine("Error: " + ex.Message);
            //}
            //Console.WriteLine("Service Host started @ " + DateTime.Now.ToString());
            //Console.Read();
        }
        private static void ServiceProc()
        {
            ServiceHost host = new ServiceHost(typeof(restservice.Service));
            try
            {
                Console.WriteLine("WCF Service: Starting Up");
                host.Opened += new EventHandler(host_Opened);
                host.Open();
                Console.WriteLine("Service Host started @ " + DateTime.Now.ToString());
                Console.Read();
            }
            finally
            {
                if (host.State != System.ServiceModel.CommunicationState.Closed)
                {
                    host.Close();
                }
            }
        }

        private static void host_Opened(object sender, EventArgs e)
        {
            Console.WriteLine("WCF Service: Running");

            System.ServiceModel.ServiceHost host = (System.ServiceModel.ServiceHost)sender;
            StringBuilder detailBuilder = new StringBuilder();
            foreach (Uri baseAddress in host.BaseAddresses)
            {
                detailBuilder.AppendFormat("\r\nBase Address: {0}", baseAddress.ToString());
            }

            foreach (ServiceEndpoint endPoint in host.Description.Endpoints)
            {
                string address = endPoint.Address.ToString();
                string bindingName = endPoint.Binding.Name;
                detailBuilder.AppendFormat("\r\nEndpoint({0}): {1}", bindingName, address);
            }

            Console.WriteLine(detailBuilder.ToString());
        }
    }
}
